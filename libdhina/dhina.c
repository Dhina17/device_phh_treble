#include <android/log.h>

#define TAG "DhinaZygoteLib"

#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,    TAG, __VA_ARGS__)

// It won't load
// Just a dummy lib to load in zygote
void hello() {
   LOGD("Hello Dhina17 from Zygote Process");
}
